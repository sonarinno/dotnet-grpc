﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using Helloworld;

namespace Client
{
    class Program
    {
        const string address = "localhost";
        const int port = 50051;

        static async Task Main(string[] args)
        {
            Console.WriteLine("Start Client!!!");

            Channel channel = new Channel(address + ":" + port.ToString(), ChannelCredentials.Insecure);
            var client = new Greeter.GreeterClient(channel);
            var reply = client.SayHello(
                              new HelloRequest { Name = "GreeterClient" });
            Console.WriteLine("Greeting: " + reply.Message);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();

            await channel.ShutdownAsync();
        }
    }
}
