# DotNet-GRPC

## Tools
1. Visual studio
## Protocol buffers
1. Create Protobuf Class Libray (.Net Standard) project
1. Install Packages with NuGet
    1. Google.Protobuf
    1. GRPC
    1. GRPC.Tools
1. Set Build Action in properties to Protobuf compiler
1. Build to DLL file

## Server
1. Create Server Console App (.Net Framwork) project
1. Install Packages with NuGet
    1. Google.Protobuf
    1. GRPC.Core
1. Add referece to Protobuf.dll file from Protobuf project
    1. Add new Lib folder
    1. Copy Protobuf.dll to Lib folder
    1. Add reference to Protobuf.dll  
1. Implementing HelloWorldImpl.cs
>```csharp
>using System;
>using System.Threading.Tasks;
>using Grpc.Core;
>using Helloworld;
>
>namespace Server
>{
>    class HelloWorldImpl : Greeter.GreeterBase
>    {
>        public override Task<HelloResponse> SayHello(HelloRequest request, ServerCallContext context)
>        {
>            Console.WriteLine("Hello " + request.Name);
>
>            var response = new HelloResponse {
>                Message = "Hello " + request.Name
>            };
>
>            return Task.FromResult(response);
>        }
>
>        public override Task<HelloResponse> SayHelloAgain(HelloRequest request, ServerCallContext context)
>        {
>            Console.WriteLine("Hello " + request.Name);
>
>            var response = new HelloResponse
>            {
>                Message = "Hello " + request.Name
>            };
>
>            return Task.FromResult(response);
>        }
>    }
>}
>```
5. Implementing Program.cs
>```csharp
>using System;
>using Grpc.Core;
>using Helloworld;
>
>namespace Server
>{
>    class Program
>    {
>        const string address = "localhost";
>        const int port = 50051;
>
>        static void Main(string[] args)
>        {
>            Console.WriteLine("Start Server!!!");
>
>            Grpc.Core.Server server = new Grpc.Core.Server
>            {
>                Services = {
>                        Greeter.BindService(new HelloWorldImpl())
>                    },
>                Ports = { new ServerPort(address, port, ServerCredentials.Insecure) }
>            };
>            server.Start();
>            Console.WriteLine("GRPC server listening on port " + port);
>            Console.WriteLine("Press any key to exit...");
>            Console.ReadKey();
>            server.ShutdownAsync().Wait();
>        }
>    }
>}
>```
## Client
1. Create Client Console App (.Net Core) project
1. Install Packages with NuGet
    1. Google.Protobuf
    1. GRPC.Core
1. Add referece to Protobuf.dll file from Protobuf project
    1. Add new Lib folder
    1. Copy Protobuf.dll to Lib folder
    1. Add reference to Protobuf.dll  
1. Implementing Program.cs
>```csharp
>using System;
>using System.Threading.Tasks;
>using Grpc.Core;
>using Helloworld;
>
>namespace Client
>{
>    class Program
>    {
>        const string address = "localhost";
>        const int port = 50051;
>
>        static async Task Main(string[] args)
>        {
>            Console.WriteLine("Start Client!!!");
>
>            Channel channel = new Channel(address + ":" + port.ToString(), ChannelCredentials.Insecure);
>            var client = new Greeter.GreeterClient(channel);
>            var reply = client.SayHello(
>                              new HelloRequest { Name = "GreeterClient" });
>            Console.WriteLine("Greeting: " + reply.Message);
>            Console.WriteLine("Press any key to exit...");
>            Console.ReadKey();
>
>            await channel.ShutdownAsync();
>        }
>    }
>}
>```
## Test
Start Server\
Start Client

### Server response
>Start Server!!!\
>GRPC server listening on port 50051\
>Press any key to exit...\
>Hello GreeterClient
### Client response
>Start Client!!!\
>Greeting: Hello GreeterClient\
>Press any key to exit...