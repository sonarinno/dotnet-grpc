﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using Helloworld;

namespace Server
{
    class HelloWorldImpl : Greeter.GreeterBase
    {
        public override Task<HelloResponse> SayHello(HelloRequest request, ServerCallContext context)
        {
            Console.WriteLine("Hello " + request.Name);

            var response = new HelloResponse {
                Message = "Hello " + request.Name
            };

            return Task.FromResult(response);
        }

        public override Task<HelloResponse> SayHelloAgain(HelloRequest request, ServerCallContext context)
        {
            Console.WriteLine("Hello " + request.Name);

            var response = new HelloResponse
            {
                Message = "Hello " + request.Name
            };

            return Task.FromResult(response);
        }
    }
}
