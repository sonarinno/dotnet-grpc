﻿using System;
using Grpc.Core;
using Helloworld;

namespace Server
{
    class Program
    {
        const string address = "localhost";
        const int port = 50051;

        static void Main(string[] args)
        {
            Console.WriteLine("Start Server!!!");

            Grpc.Core.Server server = new Grpc.Core.Server
            {
                Services = {
                        Greeter.BindService(new HelloWorldImpl())
                    },
                Ports = { new ServerPort(address, port, ServerCredentials.Insecure) }
            };
            server.Start();
            Console.WriteLine("GRPC server listening on port " + port);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
            server.ShutdownAsync().Wait();
        }
    }
}
